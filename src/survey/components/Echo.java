package survey.components;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/echo")
public class Echo extends HttpServlet {
	// header imports jquery, bootstrap and application css and js
	String header = "<head>\n" + 
			"    <meta http-equiv=\"Content-Type\" content=\"text/html\">\n" + 
			"    <title>Survey</title>\n" + 
			"    <link rel=\"stylesheet\" href=\"survey.css\" type=\"text/css\" /> \n" + 
			"    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css\">\n" + 
			"\n" + 
			"    <script src=\"https://code.jquery.com/jquery-2.1.3.js\"></script>\n" + 
			"    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js\"></script>\n" + 
			"    <script src=\"survey.js\"></script>\n" + 
			"</head>";
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}
		
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, String[]> submittedData = request.getParameterMap();
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter writer = response.getWriter();
		writer.println("<!DOCTYPE html>");
		writer.println(header);
		writer.print("<body><div class=\"container\">");
		for(String name : submittedData.keySet()) {
			writer.print("<div class=\"row\">");
			writer.print("<span class=\"label label-primary\">" + name + "</span>");
			for(String value : submittedData.get(name)) {
				writer.print("<span class=\"label label-warning\">" + value + "</span>");
			}
			writer.print("</div>");
		}
		writer.print("</div></body></html>");
	}
}
