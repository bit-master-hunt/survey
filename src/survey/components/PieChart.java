package survey.components;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import survey.dao.SurveyDao;

@WebServlet("/piechart")
public class PieChart extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		SurveyDao data = Submit.getDatabase();
		
		response.setContentType("image/png");
		OutputStream fout = response.getOutputStream();		
		
		try {
			int numMales = data.getMaleRespondantCount();
			int numFemales = data.getNumberOfFemales();
			BufferedImage image = new BufferedImage(550, 550, BufferedImage.TYPE_INT_RGB);
			Graphics2D g = (Graphics2D)image.getGraphics();
			g.setColor(Color.lightGray);
			g.fillRect(0, 0, 550, 550);
			if(numMales != 0 || numFemales != 0) {
				double percentMale = numMales / (double)(numMales + numFemales);
				int angle = (int)Math.round(percentMale * 360);
				g.setColor(Color.blue); g.fillArc(25,  25,  500, 500,  0,  angle);
				g.setColor(Color.pink); g.fillArc(25,  25,  500,  500,  angle,  360-angle);
			}
			ImageIO.write(image, "PNG", fout);
		} catch(Exception e) {
			
		} finally {
			fout.close();
		}
	}
}
