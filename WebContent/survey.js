var threeFavorites = function () {
    return $('[name=favorites]:checked').length == 3;
}

var genderChecked = function () {
    return $('[name=gender]:checked').length == 1;
}

var isValid = function() {
    var valid = threeFavorites() && genderChecked();
    console.log(valid);
    return valid;
}
