package survey.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import survey.model.Gender;
import survey.model.Response;
import survey.model.Topic;

public class SurveyDao {
	private static List<Response> responses = new ArrayList<Response>();
	
	public void addResponse(Response r){ 
		responses.add(r);
	}
	
	public Map<Topic, Integer> getFavoritesTotals() {
		Map<Topic, Integer> summary = new HashMap<>();
		for(Topic t : Topic.values()) {
			summary.put(t, 0);
		}
		
		for(Response r : responses) {
			for(Topic t : r.getFavorites()) {
				summary.put(t, summary.get(t) + 1);
			}
		}
		return summary;
	}
	
	public int getRespondantCount() {
		return responses.size();
	}
	
	public int getMaleRespondantCount() {
		int males = 0;
		for(Response r : responses) {
			if(r.getGender() == Gender.MALE) males++;
		}
		return males;
	}
	
	public int getNumberOfFemales() {
		int females = 0;
		for(Response r : responses) {
			if(r.getGender() == Gender.FEMALE) females++;
		}
		return females;
	}
}
