package survey.model;

import java.util.List;

public class Response {
	private List<Topic> favorites;
	private Gender gender;
	
	public List<Topic> getFavorites() {
		return favorites;
	}
	
	public void setFavorites(List<Topic> favorites) {
		this.favorites = favorites;
	}
	
	public Gender getGender() {
		return gender;
	}
	
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	private Response(Builder builder) {
		this.gender = builder.gender;
		this.favorites = builder.favorites;
	}
	
	public static class Builder {
		private List<Topic> favorites;
		private Gender gender;
		
		public Builder() {}
		
		public Builder favorites(List<Topic> favorites) {
			this.favorites = favorites;
			return this;
		}
		
		public Builder gender(Gender gender) {
			this.gender = gender;
			return this;
		}
		
		public Response build() {
			return new Response(this);
		}
	}
}
