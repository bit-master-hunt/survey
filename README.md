# Description#

This application was covered in class.  It distributes surveys to anyone who wants to take the survey (users can even 
take it multiple times).  The application collects the results and responds to each submission with a page that summarizes
the cumulative results.

The application fails to support multiple submissions and is not persistent.  The application uses only bare-bones JEE technologies (servlets with @WebServlet annotation)

### What is this repository for? ###

* How to write a web app using servlets
* Repository related to in-class presentations