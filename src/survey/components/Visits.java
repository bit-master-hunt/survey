package survey.components;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("serial")
@WebServlet("/visits")
public class Visits extends HttpServlet {
	public static final String VISIT_KEY = "visitcounter.visits";
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter fout = response.getWriter();
		response.setContentType("text/html");
		
		HttpSession session = request.getSession();
		if(session.isNew()) {
			session.setAttribute(VISIT_KEY, 0);
		}
		Integer count = ((Integer)session.getAttribute(VISIT_KEY)) + 1;
		session.setAttribute(VISIT_KEY, count);
		fout.write("<!DOCTYPE html>");
		fout.write("<html><head><title></title></head><body>");
		fout.write("You have visited this page " + count + " time" + (count > 1 ? "s" : ""));
		fout.write("</body></html>");
	}
}
