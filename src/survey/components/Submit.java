package survey.components;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import survey.dao.SurveyDao;
import survey.model.Gender;
import survey.model.Response;
import survey.model.Topic;

@SuppressWarnings("serial")
@WebServlet("/submit")
public class Submit extends HttpServlet {
	private static SurveyDao database = new SurveyDao();
	
	public static SurveyDao getDatabase() {
		return database;
	}
	
	
	private List<Topic> toTopics(String[] strs) {
		List<Topic> topics = new ArrayList<Topic>();
		for(String str : strs) {
			topics.add(Topic.valueOf(str));
		}
		return topics;
	}
		
	// header imports jquery, bootstrap and application css and js
	String header = "<head>\n" + 
			"    <meta http-equiv=\"Content-Type\" content=\"text/html\">\n" + 
			"    <title>Survey</title>\n" + 
			"    <link rel=\"stylesheet\" href=\"survey.css\" type=\"text/css\" /> \n" + 
			"    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css\">\n" + 
			"\n" + 
			"    <script src=\"https://code.jquery.com/jquery-2.1.3.js\"></script>\n" + 
			"    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js\"></script>\n" + 
			"    <script src=\"survey.js\"></script>\n" + 
			"</head>";	
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] favs = request.getParameterValues("favorites");
		Gender gender = Gender.valueOf(request.getParameter("gender"));
		
		List<Topic> topics = toTopics(favs);
		Response r = new Response.Builder()
			.gender(gender)
			.favorites(topics)
			.build();
		
		database.addResponse(r);
		
		int numberOfMales = database.getMaleRespondantCount();
		int numberOfFemales = database.getNumberOfFemales();
		Map<Topic, Integer> counts = database.getFavoritesTotals();
		
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter writer = response.getWriter();
		writer.println("<!DOCTYPE html>");
		writer.println(header);
		writer.print("<body><div class=\"container\">");
		writer.print("<ul>");
		for(Topic t : counts.keySet()){ 
			Integer count = counts.get(t);
			writer.println("<li>" + t + ":" + count + "</li>");
		}
		writer.print("</ul>");
		
		writer.println("<div>Number of males responding: " + numberOfMales + "</div>");
		writer.println("<div>Number of females responding: " + numberOfFemales + "</div>");
		writer.println("</body></html>");	
	}
}
